<?php

namespace App\Components\Css;

use Nette;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	private $media = 'screen';
	private $styles = [];
	private $externalStyles = [];
	private $adminStyles = [];
	private $externalAdminStyles = [];
	private $public;

	public function __construct($public)
	{
		parent::__construct();
		$this->public = $public;
	}

	public function render()
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

	public function renderAdmin()
	{
		$this->template->render(__DIR__ . '/templates/default.admin.latte');
	}

	/*protected function createComponentCss()
	{
		$files = new \WebLoader\FileCollection;
		$files->addRemoteFiles($this->externalStyles);
		$files->addFiles($this->styles);
		$compiler = \WebLoader\Compiler::createCssCompiler($files, $this->public . '/temp');
		$compiler->addFileFilter(new \WebLoader\Filter\LessFilter);
		$compiler->addFilter(function ($code) {
			$minifier = new \MatthiasMullie\Minify\CSS;
			$minifier->add($code);
			return $minifier->minify();
		});
		$control = new \WebLoader\Nette\CssLoader($compiler, $this->template->basePath . '/temp');
		$control->setMedia($this->media);
		return $control;
	}

	protected function createComponentCssAdmin()
	{
		$files = new \WebLoader\FileCollection;
		$files->addRemoteFiles($this->externalAdminStyles);
		$files->addFiles($this->adminStyles);
		$compiler = \WebLoader\Compiler::createCssCompiler($files, $this->public . '/temp');
		$compiler->addFileFilter(new \WebLoader\Filter\LessFilter);
		$compiler->addFilter(function ($code) {
			$minifier = new \MatthiasMullie\Minify\CSS;
			$minifier->add($code);
			return $minifier->minify();
		});
		$control = new \WebLoader\Nette\CssLoader($compiler, $this->template->basePath . '/temp');
		$control->setMedia($this->media);
		return $control;
	}*/

	public function setMedia(array $media)
	{
		$this->media = implode(',', array_unique($media));
	}

}
