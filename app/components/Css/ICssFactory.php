<?php

namespace App\Components\Css;

interface ICssFactory
{

	/** @return Component */
	function create();
}
