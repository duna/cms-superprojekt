<?php

namespace App\Components\Title;

use Nette;
use Duna\Core\Options\OptionFacade;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	private $title = null;

	/** @var OptionFacade */
	private $optionFacade;

	public function __construct(OptionFacade $facade)
	{
		parent::__construct();
		$this->optionFacade = $facade;
	}

	public function render($title = null)
	{
		if ($title !== null)
			$this->setTitle($title);

		$template = $this->template;
		$template->title = $this->getTitle();
		$template->render(__DIR__ . '/templates/default.latte');
	}

	public function setTitle($title)
	{
		if ($this->title !== null)
			throw new Nette\InvalidStateException(sprintf('You are trying to override current title (%s).', $this->getTitile()));
		$this->title = $title;
	}

	public function getTitle()
	{
		$separator = $this->optionFacade->getOption('site_title_separator');
		if ($separator === null)
			$separator = $this->title ? '|' : null;
		else
			$separator = $this->title ? $separator : null;

		$siteTitle = $this->optionFacade->getOption('site_title');
		return "$this->title $separator $siteTitle";
	}

}
