<?php

namespace App\Components\Title;

interface ITitleFactory
{

	/** @return Component */
	function create();
}
