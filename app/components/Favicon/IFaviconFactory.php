<?php

namespace App\Components\Favicon;

interface IFaviconFactory
{

	/** @return Component */
	function create();
}
