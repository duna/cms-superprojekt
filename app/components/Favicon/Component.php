<?php

namespace App\Components\Favicon;

use Nette;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	public function render()
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

}
