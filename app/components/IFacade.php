<?php

namespace App\Components;

interface IFacade
{

	public function getById($id, $throwExceptions = true);

	public function insert(array $data, $throwExceptions = true, $autoFlush = false);

	public function delete($id, $autoFlush = false);

	public function update($id, array $data, $throwExceptions = true, $autoFlush = false);
}
