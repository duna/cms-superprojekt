<?php

namespace App\Components\Flash;

use Nette\Application\UI\Control;
use Nette\Utils\ArrayHash;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\Components\Flash
 */
class Flash extends Control
{

    const SUCCESS = 'success';
    const INFO = 'info';
    const WARNING = 'warning';
    const DANGER = 'danger';

    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);
    }

    public function render(array $parameters = null)
    {
        $template = $this->template;

        if ($parameters) {
            $template->parameters = ArrayHash::from($parameters);
        }
        $flashes = $this->getPresenter()->getTemplate()->flashes;
        $template->flashes = $flashes;
        $template->render(__DIR__ . '/templates/default.latte');
    }

}
