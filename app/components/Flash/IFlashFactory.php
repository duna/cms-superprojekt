<?php

namespace App\Components\Flash;

interface IFlashFactory
{

    /** @return Flash */
    function create($parent, $name);
}
