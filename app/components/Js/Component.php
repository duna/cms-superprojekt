<?php

namespace App\Components\Js;

use Nette;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	private $scripts = [];
	private $externalScripts = [];
	private $adminScripts = [];
	private $externalAdminScripts = [];
	private $public;

	public function __construct($public)
	{
		parent::__construct();
		$this->public = $public;
	}

	public function render()
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

	public function renderAdmin()
	{
		$this->template->render(__DIR__ . '/templates/default.admin.latte');
	}
/*
	protected function createComponentJs()
	{
		$files = new \WebLoader\FileCollection;
		$files->addRemoteFiles($this->externalScripts);
		$files->addFiles($this->scripts);
		$compiler = \WebLoader\Compiler::createJsCompiler($files, $this->public . '/temp');
		$compiler->addFilter(function ($code) {
			$minifier = new \MatthiasMullie\Minify\Js;
			$minifier->add($code);
			return $minifier->minify();
		});
		$control = new \WebLoader\Nette\JavaScriptLoader($compiler, $this->template->basePath . '/temp');
		return $control;
	}

	protected function createComponentJsAdmin()
	{
		$files = new \WebLoader\FileCollection;
		$files->addRemoteFiles($this->externalAdminScripts);
		$files->addFiles($this->adminScripts);
		$compiler = \WebLoader\Compiler::createJsCompiler($files, $this->public . '/temp');
		$compiler->addFilter(function ($code) {
			$minifier = new \MatthiasMullie\Minify\Js;
			$minifier->add($code);
			return $minifier->minify();
		});
		$control = new \WebLoader\Nette\JavaScriptLoader($compiler, $this->template->basePath . '/temp');

		return $control;
	}*/

}
