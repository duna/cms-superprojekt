<?php

namespace App\Components\Js;

interface IJsFactory
{

	/** @return Component */
	function create();
}
