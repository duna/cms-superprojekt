<?php

namespace App\Components;

use Kdyby\Doctrine\EntityManager;

/**
 * @author Lukáš Černý <LukasCerny@hotmail.com>
 */
class BaseFacade
{

    /** \Kdyby\Doctrine\EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

}
