<?php

namespace App\Components\SignInForm;

interface ISignInFormFactory
{

    /**
     * @param \Nette\Application\UI\Control $parent
     * @param $name
     * @return \App\Components\SignInForm\Component
     */
    function create($parent, $name);
}
