<?php

namespace App\Components\SignInForm;

use Duna\Forms\ControlForm;
use Duna\Forms\Form;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\Components\SignInForm
 */
class Component extends ControlForm
{
    public function __construct($parent, $name, EntityManager $em)
    {
        parent::__construct($parent, $name, $em, null);
    }

    public function render()
    {
        $this->template->render(__DIR__ . '/templates/default.latte');
    }

    public function formSuccess(Form $form, $values)
    {
        $values = $form->getValues();

        if ($values->remember)
            $this->getPresenter()->getUser()->setExpiration('+ 14 days', false);
        else
            $this->getPresenter()->getUser()->setExpiration('+ 20 minutes', true);
        try {
            $this->getPresenter()->getUser()->login($values->email, $values->password);
        } catch (Nette\Security\AuthenticationException $ex) {
            $form->addError($ex->getMessage());
            return;
        }
    }

    public function formError(Form $form)
    {
        // TODO: Implement formError() method.
    }

    public function setDefaults(Form $form)
    {
        // TODO: Implement setDefaults() method.
    }

    protected function createComponentForm($name)
    {
        $form = $this->createForm($name);
        $form->addText('email', 'Email')
            ->setRequired('Prosím zadejte Váš email.');
        $form->addPassword('password', 'Heslo')
            ->setAttribute('autocomplete', 'off')
            ->setRequired('Zadejte prosím Vaše heslo');
        $form->addCheckbox('remember', 'Trvalé přihlášení');
        $form->addSubmit('send', 'Přihlásit');
        return $form;
    }


}
