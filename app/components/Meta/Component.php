<?php

namespace App\Components\Meta;

use Nette;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	private $robots = [];
	private $metas = [
		'author' => 'Lukáš Černý (LukasCerny@hotmail.com)',
		'robots' => 'all'
	];
	private $httpEquivs = [];

	public function render()
	{
		$template = $this->template;
		if (count(array_filter($this->robots))) {
			$this->metas['robots'] = implode(', ', array_filter($this->robots));
		}
		$template->metas = $this->metas;
		$template->httpEquivs = $this->httpEquivs;
		$template->render(__DIR__ . '/templates/default.latte');
	}

	public function setRobots($value)
	{
		if (is_array($value)) {
			foreach ($value as $val) {
				$this->robots[$val] = $val;
			}
		} else
			$this->robots[$value] = $value;
	}

	public function setMeta($name, $content)
	{
		if (!empty($content))
			$this->metas[$name] = $content;
	}

	public function setMetas(array $metas)
	{
		foreach ($metas as $name => $content) {
			$this->setMeta($name, $content);
		}
	}

	public function setHttpEquiv($httpEquiv, $content)
	{
		if (in_array(mb_strtolower($httpEquiv), [
					'content-type', 'content-language', 'x-ua-compatible'
				])) {
			throw new Nette\InvalidArgumentException(sprintf('You are not allowed to take care about %s meta tag. I will do it for you.', $httpEquiv));
		}
		$this->httpEquivs[$httpEquiv] = $content;
	}

}
