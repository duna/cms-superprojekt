<?php

namespace App\Components\Meta;

interface IMetaFactory
{

	/** @return Component */
	function create();
}
