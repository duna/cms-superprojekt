<?php

namespace App\Components\Doctype;

use Nette;

/**
 * @author Lukáš Černý <lukas.cerny@hotmail.com>
 */
class Component extends Nette\Application\UI\Control
{

	private $selected = Types::HTML5;

	public function render()
	{
		$template = $this->template;
		$template->doctype = Nette\Utils\Html::el()->setHtml($this->selected);
		$template->render(__DIR__ . '/templates/default.latte');
	}

	public function setDoctype(Types $type)
	{
		if (Types::exists($type))
			$this->selected = $type;
		else
			throw new Nette\InvalidArgumentException('Inknown doctype');
	}

}
