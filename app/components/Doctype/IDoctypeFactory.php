<?php

namespace App\Components\Doctype;

interface IDoctypeFactory
{

	/** @return Component */
	function create();
}
