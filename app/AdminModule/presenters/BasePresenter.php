<?php

namespace App\AdminModule\Presenters;

use App;
use Duna\Security\Authorizator;
use Nette\Security\IUserStorage;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\AdminModule\Presenters
 */
abstract class BasePresenter extends App\Presenters\BasePresenter
{

    public function checkRequirements($element)
    {
        parent::checkRequirements($element);
        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === IUserStorage::INACTIVITY) {
                $this->flashMessage("Byl jste odhlášen z důvodu nečinnosti. Přihlaste se prosím znovu.", App\Components\Flash\Flash::DANGER);
            } else {
                $this->flashMessage("Pro vstup do této sekce se musíte přihlásit.", App\Components\Flash\Flash::DANGER);
            }
            $this->redirect(':Auth:Sign:in', ['backlink' => $this->storeRequest()]);
        } elseif (!$this->user->isAllowed($this->name, Authorizator::READ)) {
            $this->flashMessage("Přistup byl odepřen. Nemáte oprávnění k zobrazení této stránky.", App\Components\Flash\Flash::DANGER);
            $this->redirect(':Auth:Sign:in', ['backlink' => $this->storeRequest()]);
        }
    }

    public function findLayoutTemplateFile()
    {
        return ($this->layout === false) ?: __DIR__ . '/templates/@layout.latte';
    }

}
