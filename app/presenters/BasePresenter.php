<?php

namespace App\Presenters;

use App\Traits\PublicComponentsTrait;
use Kdyby\Doctrine\EntityManager;
use Nette;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\Presenters
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    /** @persistent */
    public $locale;

    /** @persistent */
    public $backlink;

    /** @var EntityManager @inject */
    public $em;

    /** @var \Duna\Core\Options\OptionFacade @inject */
    public $optionFacade;

    use PublicComponentsTrait;

    protected function beforeRender()
    {
        $this->template->locale = $this->locale;
        $this->setMeta('og:url', $this->link('//this'));
        $this->setMeta('og:site_name', $this->optionFacade->getOption('site_name'));
    }

    protected function setMeta($name, $content)
    {
        $this['meta']->setMeta($name, $content);
    }

    protected function startup()
    {
        parent::startup();
        if ($this->isAjax()) {
            $this['flash']->redrawControl();
        }
    }

    protected function setTitle($title)
    {
        $this['title']->setTitle($title);
    }

}
