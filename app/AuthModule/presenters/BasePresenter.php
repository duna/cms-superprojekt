<?php

namespace App\AuthModule\Presenters;

use App;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\AuthModule\Presenters
 */
abstract class BasePresenter extends App\Presenters\BasePresenter
{

}
