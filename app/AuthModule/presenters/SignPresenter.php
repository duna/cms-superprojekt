<?php

namespace App\AuthModule\Presenters;

use App\Components\Flash\Flash;
use App\Components\SignInForm\ISignInFormFactory;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package App\AuthModule\Presenters
 */
class SignPresenter extends BasePresenter
{

    public function actionOut()
    {
        $this->getUser()->logout(true);
        $this->flashMessage('Odhlášení proběhlo úspěšně.', Flash::INFO);
        $this->restoreRequest($this->backlink);
        $this->redirect('in');
    }

    protected function startup()
    {
        parent::startup();
        if ($this->action != 'out' && $this->user->isLoggedIn()) {
            $this->redirect(':Dashboard:Front:Dashboard:');
        }
    }

    protected function createComponentSignInForm($name, ISignInFormFactory $factory)
    {
        $form = $factory->create($this, $name);
        $that = $this;
        $form->onSuccess[] = function () use ($that) {
            $that->restoreRequest($this->backlink);
            $that->redirect(':Dashboard:Front:Dashboard:');
        };
        return $form;
    }

}
