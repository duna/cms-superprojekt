<?php

$loader = require __DIR__ . '/../vendor/autoload.php';

$configurator = new \Duna\Core\Configurator($loader);

$configurator->enableTracy(__DIR__ . '/../var/log');
$configurator->setTempDirectory(__DIR__ . '/../var/temp');

$configurator->setTimeZone('Europe/Prague');

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
