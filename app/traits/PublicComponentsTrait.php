<?php

namespace App\Traits;

use App\Components;
use Duna\Core\Navigation\Components\IAdminMenuFactory;
use Duna\Core\Navigation\Components\IMainMenuFactory;
use Kdyby;

trait PublicComponentsTrait
{

    use Kdyby\Autowired\AutowireComponentFactories;

    protected function createComponentDoctype(Components\Doctype\IDoctypeFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentMeta(Components\Meta\IMetaFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentTitle(Components\Title\ITitleFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentCss(Components\Css\ICssFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentFavicon(Components\Favicon\IFaviconFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentJs(Components\Js\IJsFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentMainMenu(IMainMenuFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentAdminMenu(IAdminMenuFactory $factory)
    {
        return $factory->create();
    }

    protected function createComponentFlash($name, Components\Flash\IFlashFactory $factory)
    {
        return $factory->create($this, $name);
    }

}
